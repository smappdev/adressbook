package com.company.controller;

import com.company.manager.CommandParser;
import com.company.manager.ContactsManager;
import com.company.model.Command;
import com.company.model.Contact;
import com.company.model.Contacts;
import com.company.utils.FileManager;
import com.company.view.Message;
import com.company.view.Prompt;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AddressBook {

    Contacts contacts = new Contacts();
    FileManager fileManager = new FileManager();
    List<String> names = new ArrayList<>();
    List<String> phones = new ArrayList<>();


    public void start(){
        Message.printTitle();
        loadData();
        boolean end = false;
        while (!end){
            Prompt.print(contacts);
            String command = Prompt.read();

            Command com = CommandParser.parse(command);

            switch (com){
                case ADD:
                    ContactsManager.add(contacts);
                    break;

                case DELETE:
                    ContactsManager.delete(contacts);
                    break;

                case LIST:
                    contacts.print();
                    break;

                case HELP:
                    Message.printHelp();
                    break;

                case QUIT:
                    List<String> contactNames = new ArrayList<>();
                    List<String> contactPhones = new ArrayList<>();
                    LinkedList<Contact> contactsToSave = contacts.getContacts();

                    for (Contact contact : contactsToSave) {
                        contactNames.add(contact.getName());
                        contactPhones.add(contact.getPhone());
                    }
                    saveData(contactNames, "names.txt");
                    saveData(contactPhones, "phones.txt");
                    end = true;
                    break;

                case UNKNOWN:
                    System.out.println("Comando erróneo. Escriba (H)ELP para mostrar la ayuda.");
                    break;
            }
        }
    }

    private void loadData() {
        try{
            names = fileManager.readFile("names.txt");
            phones = fileManager.readFile("phones.txt");

            for (int i = 0; i < names.size(); i++) {
                Contact contactToRead = new Contact(names.get(i), phones.get(i));
                contacts.add(contactToRead);
            }
        }catch (NullPointerException e){
            System.out.println("Creando archivos...");
        }
    }

    private void saveData(List<String> contactData, String fileToSave) {
        try {
            fileManager.createFile(fileToSave, contactData);
        } catch (IOException e) {
            System.out.println("Error grabando archivo de datos...");
            e.printStackTrace();
        }
    }
}
