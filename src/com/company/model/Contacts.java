package com.company.model;

import java.util.LinkedList;

public class Contacts {

    private LinkedList<Contact> contacts = new LinkedList<>();

    public Contacts() {
    }

    public void print(){
        if (contacts.isEmpty()){
            System.out.println("La agenda está vacía.");
            return;
        }
        System.out.println("Lista de contactos:");
        for (Contact contact: contacts) {
            System.out.println(contacts.indexOf(contact) + " - " + contact.getName() + " - " + contact.getPhone());
        }
        System.out.println();
    }

    public void add(Contact contact){
        this.contacts.add(contact);
    }

    public LinkedList<Contact> getContacts() {
        return contacts;
    }

    public boolean isEmpty(){
        return contacts.size() == 0;
    }


}
