package com.company.view;


import com.company.model.Contacts;

import java.util.Scanner;

public class Prompt {

    public static void print(Contacts contacts){
        System.out.print("(" + contacts.getContacts().size() + ") -> ");
    }

    public static String read() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static void cls(){
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }
}