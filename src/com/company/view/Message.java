package com.company.view;

public class Message {
    public static void printTitle(){
        System.out.println("Bienvenido a tu agenda personal. ¿Qué desea hacer?");
    }

    public static void printHelp(){
        String[] help = {
                "  _  _ ___ _    ___ \n" +
                        " | || | __| |  | _ \\\n" +
                        " | __ | _|| |__|  _/\n" +
                        " |_||_|___|____|_|  \n" +
                        "                    ",
                "(H)ELP \t-> Muestra la ayuda",
                "(Q)UIT \t-> Salir del programa",
                "\nAGENDA:\n",
                "(A)DD \t\t-> Añadir un contacto nuevo",
                "(D)ELETE \t-> Eliminar una entrada de la agenda",
                "(L)IST \t\t-> Muestra la agenda\n",

        };
        for (int i = 0; i < help.length; i++) {
            System.out.println(help[i]);
        }
    }
}
