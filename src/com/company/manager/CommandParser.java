package com.company.manager;

import com.company.model.Command;

public class CommandParser {

    public static Command parse(String text){

        if (text.equalsIgnoreCase("Help") || text.equalsIgnoreCase("h")) {
            return Command.HELP;
        }
        if (text.equalsIgnoreCase("Quit") || text.equalsIgnoreCase("q")) {
            return Command.QUIT;
        }
        if (text.equalsIgnoreCase("List") || text.equalsIgnoreCase("l")) {
            return Command.LIST;
        }
        if (text.equalsIgnoreCase("Add") || text.equalsIgnoreCase("a")) {
            return Command.ADD;
        }
        if (text.equalsIgnoreCase("Delete") || text.equalsIgnoreCase("d")) {
            return Command.DELETE;
        }
        return Command.UNKNOWN;
    }
}