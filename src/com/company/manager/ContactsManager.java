package com.company.manager;

import com.company.model.Contact;
import com.company.model.Contacts;

import java.util.LinkedList;
import java.util.Scanner;

public class ContactsManager {
    public static void add(Contacts contacts){
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nombre: ");
        String name = scanner.nextLine();

        System.out.print("Telefono: ");
        String phone = scanner.nextLine();

        if (name.equals("") || phone.equals("")){
            System.out.println("Faltan datos");
        } else {
            System.out.println("Contacto añadido...");
            Contact newContact = new Contact(name, phone);
            contacts.add(newContact);
        }
    }

    public static void delete(Contacts contacts){
        Scanner scanner = new Scanner(System.in);

        if (contacts.isEmpty()){
            System.out.println("La agenda está vacía.");
            return;
        }

        System.out.println("Introduza registro a borrar: ");
        String position = scanner.nextLine();

 /* ---------------------------------*/

        try {
            contacts.getContacts().remove(Integer.parseInt(position));
            System.out.println("Contacto eliminado...");
        }catch (Exception e){
            System.out.println("La posición indicada no existe.");
        }

    }
}
