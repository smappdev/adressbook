# AddressBook By Saúl Marín Querido
---
La aplicación utiliza dos archivos de texto para almacenar los datos entre sesiones, de forma que al finalizar el programa y volverlo a ejecutar los datos se mantienen.
Si dichos archivos no existen, se crearán al iniciar por primera vez la aplicación.


En la pantalla principal aparece entre paréntesis el número de registros actuales.

---

La aplicación se mantiene a la espera que el usuario introduzca uno de los siguientes comandos:

---
## Comandos disponibles:

1. ### (H)elp 
* Muestra la ayuda por pantalla
1. ### (A)dd
* Añade un contacto nuevo a la agenda. Es necesario introducir nombre y teléfono.
* El contacto añadido se sitúa al final de la agenda.
1. ### (D)elete
* Se solicita que se introduzca el número de la posición del registro que se desea eliminar. Si se proporciona una posición incorrecta se vuelve al menú inicial.
1. ### (L)ist
* Muestra por pantalla el contenido de la agenda. A la izquierda de cada registro aparece un numero indicando la posición en la que se encuentran.
* Dicho número es el que se debe pasar cuando se intenta borrar un registro.
1. ### (Q)uit
* Se guardan los datos en los ficheros correspondientes y se finaliza el programa.